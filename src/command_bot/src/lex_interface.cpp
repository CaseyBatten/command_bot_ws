/*
 *
 * The following client node interfaces with aws_lex on behalf of the other nodes, publishing its data to the event runner
 * Publishes findings to the "intents" topic
 * Can be triggered by a /input topic containing the user message in text, or a /voice topic containing the filepath of an audio sample
 * 
 */
#include <lex_node/lex_node.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <cstdlib>
#include <sstream>
#include <fstream> 
#include <vector>
#include <iostream>

ros::ServiceClient client;
ros::Publisher intent_pub;

void voiceCallback(const std_msgs::String::ConstPtr& msg){
    //msg data contains audio file path
    lex_common_msgs::AudioTextConversation srv;
    srv.request.content_type = "audio/l16; rate=16000; channels=1";
    srv.request.accept_type = "text/plain; charset=utf-8";
    
    std::vector<uint8_t> data;
    std::ifstream file (msg->data, std::ios::binary);
    
    // check the file exists
    if (! file.good())
    {
        ROS_INFO("ERROR: File doesn't exist or otherwise can't load file\n");
    }
    
	file.unsetf (std::ios::skipws);

	file.seekg (0, std::ios::end);
	size_t length = file.tellg();
	file.seekg (0, std::ios::beg);

	// allocate
	data.resize (length);

	file.read(reinterpret_cast<char*> (data.data()), length);
	file.close();
        
    srv.request.audio_request.data = data;
    
    ros::Duration t = (ros::Duration)5;
    if(client.waitForExistence(t)){
        if (client.call(srv))
        {
            ROS_INFO("Intent from lex_node: %s", srv.response.intent_name.c_str());
            ROS_INFO("Chatbot says: %s", srv.response.text_response.c_str());
            ROS_INFO("count");
            std_msgs::String msg; 
            std::stringstream ss;
            ss << srv.response.intent_name;
            msg.data = ss.str();
            intent_pub.publish(msg);
        }
        else
        {
            ROS_INFO("Failed to call service lex node");
        }
    }
}

void inputCallback(const std_msgs::String::ConstPtr& msg)
{
    lex_common_msgs::AudioTextConversation srv;
    srv.request.content_type = "text/plain; charset=utf-8";
    srv.request.accept_type = "text/plain; charset=utf-8";
    srv.request.text_request = msg->data;
    
    //replace these with a lex request
    ros::Duration t = (ros::Duration)5;
    if(client.waitForExistence(t)){
        if (client.call(srv))
        {
            ROS_INFO("Intent from lex_node: %s", srv.response.intent_name.c_str());
            ROS_INFO("Chatbot says: %s", srv.response.text_response.c_str());
            std_msgs::String msg; 
            std::stringstream ss;
            ss << srv.response.intent_name;
            msg.data = ss.str();
            intent_pub.publish(msg);
        }
        else
        {
            ROS_INFO("Failed to call service lex node");
        }
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "lex_interface");
    

    ros::NodeHandle n;
    client = n.serviceClient<lex_common_msgs::AudioTextConversation>("/lex_node/lex_conversation");

    intent_pub = n.advertise<std_msgs::String>("/intents", 1000);
    ros::Subscriber input_sub = n.subscribe("/input", 1000, inputCallback);
    ros::Subscriber voice_sub = n.subscribe("/voice", 1000, inputCallback);

    ROS_INFO("Lex Interface Node Ready");
    ros::spin();
    return 0;
}