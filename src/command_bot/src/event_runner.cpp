/*
 *
 * This script is responsible for executing events
 * Events are detailed in the event.xml file and read in on process start
 * Events are triggered by messages recieved from amazon lex's lex_node
 * 
 */

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "event.h"
#include "tinyxml2.h"

using namespace std;
using namespace tinyxml2;

// Global state management struct
struct roboState{
    Event currEvent;
    int interactivity_level;
};
roboState state;
vector<Event> events;

void actionExecute(Parameters action){
    // Perform predetermined actions
    //To add new action types, simply add an else if statement comparing for the expected action string
    if(action.type == ""){
        //Do nothing
    }
    else if(action.type == "increase_interact"){
        state.interactivity_level+=action.val;
    }
    else{
        ROS_INFO("ERROR: No matching action for: %s", action.type.c_str());
    }
}

bool eventExecutor(Event e){
    // Perform requriements checks -
    // To add new requirements, simply add an else if statement comparing for the expected string node
    if(e.requirement.type == "interact_level"){
        if(state.interactivity_level >= e.requirement.val){
            //execute state
            state.currEvent = e;

            ROS_INFO("Interactivity check passed, transitioned to event %s", e.name.c_str());

            //Execute any actions this state has
            actionExecute(e.action);
            return true;
        }
        else{
            ROS_INFO("Failed to enter event %s", e.name.c_str());
            return false;
        }
    }
    else{
        // State has no requirements, execute
        state.currEvent = e;
        actionExecute(e.action);
        ROS_INFO("Transitioned to event %s", e.name.c_str());
        return true;
    }
}

void intentCallback(const std_msgs::String::ConstPtr& msg)
{
    // Search events for a matching intent name
    for(int i = 0; i < events.size(); i++){
        if(events.at(i).intent == msg->data){
            ROS_INFO("Event Runner received valid intent: %s", msg->data.c_str());
            if(events.at(i).name == state.currEvent.name){
                ROS_INFO("Already in event %s!", events.at(i).name.c_str());
            }
            else{
                Event t = events.at(i);
                bool result = eventExecutor(t);
                if(result){
                    ROS_INFO("Event %s executed successfully", events.at(i).name.c_str());
                }
                else{
                    ROS_INFO("Failed to enter event %s", events.at(i).name.c_str());
                }
            }
        }
    }
}

bool loadEventXML(const string& fileName){
    XMLDocument doc;

    doc.LoadFile(fileName.c_str());
    if ( doc.Error() ) {
		ROS_INFO("Error loading doc: %s\n", doc.ErrorStr());
		return false;
	}

    //sort the xml into invidual events using pushback
    XMLElement* pE;
    XMLElement* pP;
    const XMLAttribute* pA;
    Event temp;
    try{
        
        for(pE = doc.RootElement()->FirstChildElement(); pE; pE = pE->NextSiblingElement()){
            if ( !strcmp(pE->Name(), "event") ) {
                for (pA = pE->FirstAttribute(); pA; pA = pA->Next() ) {
                    if ( !strcmp(pA->Name(), "name") ) {
                        temp.name = pA->Value();
                    }
                    if( !strcmp(pA->Name(), "intent") ){
                        temp.intent = pA->Value();	
                    }
                }  
                //Handle the requirements and action sub-items
                for ( pP = pE->FirstChildElement(); pP; pP = pP->NextSiblingElement()) {
                    if ( !strcmp(pP->Name(), "requirement") ) {
                        for (pA = pP->FirstAttribute(); pA; pA = pA->Next() ) {
                            if ( !strcmp(pA->Name(), "type") ) {
                                temp.requirement.type = pA->Value();
                            }
                            if( !strcmp(pA->Name(), "value") ){
                                temp.requirement.val = atoi(pA->Value());	
                            }
                        } 
                    }
                    else if ( !strcmp(pP->Name(), "action") ) {
                        for (pA = pP->FirstAttribute(); pA; pA = pA->Next() ) {
                            if ( !strcmp(pA->Name(), "type") ) {
                                temp.action.type = pA->Value();	
                            }
                            if( !strcmp(pA->Name(), "value") ){
                                temp.action.val = atoi(pA->Value());	
                            }
                        } 
                    }
                    else {
                        ROS_INFO("ERROR: Expected 'requirement' or 'action'");
                    }
                }
            }
            else{
                ROS_INFO("ERROR: Expected 'event'");
                return false;
            }
            events.push_back(temp);
        }
    }
	catch ( const string& err ) {
		ROS_INFO("ERROR:  '%s'\n", err.c_str());
		return false;
	}

    return true;

}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "EventRunner");
    //initialize the robot state
    Event neutral;
    neutral.name="Neutral";
    neutral.intent = "__NONE__";
    neutral.requirement.type = "__NONE__";
    neutral.requirement.val = 0;
    neutral.action.type = "__NONE__";
    neutral.action.val = 0;
    state.currEvent = neutral;
    state.interactivity_level = 0; 

    //read the XML, adding all events
    //Assumes current working directory is /home/USER/.ros
    if(loadEventXML("../command_bot_ws/src/command_bot/examples/Events.xml")){
#if 0
        for(int i = 0; i < events.size(); i++){
            ROS_INFO("event %s", events.at(i).name.c_str());
            ROS_INFO("intent %s", events.at(i).intent.c_str());
            ROS_INFO("requirements %s %d", events.at(i).requirement.type.c_str(), events.at(i).requirement.val);
            ROS_INFO("actions %s %d", events.at(i).action.type.c_str(), events.at(i).action.val);
        }
#endif
        ros::NodeHandle n;

        ros::Subscriber xml_sub = n.subscribe("/intents", 1000, intentCallback);

        ROS_INFO("Event Node Ready");
        ros::spin();
    }
    else{
        ROS_INFO("ERROR: Failed to load 'Events.xml'");
    }

    return 0;
}