#include <string>
#include <vector>

struct Parameters{
    std::string type;
    int val;
};


struct Event
{
    std::string name;
    std::string intent;
    Parameters requirement;
    Parameters action;
};
