cmake_minimum_required(VERSION 3.0.2)
project(command_bot)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  lex_node
)

catkin_package(
)


## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include/${PROJECT_NAME}
  ${catkin_INCLUDE_DIRS}
)


add_executable(event_runner_node src/event_runner.cpp src/tinyxml2.cpp)
target_link_libraries(event_runner_node ${catkin_LIBRARIES} )

add_executable(lex_interface_node src/lex_interface.cpp)
target_link_libraries(lex_interface_node ${catkin_LIBRARIES} )




install(DIRECTORY include/${PROJECT_NAME}/
        DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION})
