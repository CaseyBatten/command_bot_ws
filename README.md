# COMMAND BOT - AWS Integrated Event-based ROS package

This ROS1 package  integrates with the Amazon Web Services lex-ros1 package. This package is compatible with ROS Melodic.

## Directory Details
Within this repository is the Command Bot ROS package. The **/src/command_bot** folder contains the package in its entirety.

Within that folder there is a sub-directory titled **/examples** which contains a sample AWS configuration yaml file, an example Events.xml file for bot operation, and a command_bot_Export.json file-copy of the AWS Lex chatbot model used in the example builds of this project.

## Setting Up
In order to fully utilize this ROS package the user must first add in the required AWS lex-ros1 node.
The AWS lex-ros1 node can be found here:
https://github.com/aws-robotics/lex-ros1

This project also requires tinyxml2, found here:
https://github.com/leethomason/tinyxml2


First, lets set up AWS lex-ros1:
```
git clone https://gitlab.com/CaseyBatten/command_bot_ws
cd ~/command_bot_ws/src
git clone https://github.com/aws-robotics/lex-ros1.git -b release-latest

```

Step three involved getting the tinyxml2 files needed for standard operation. Clone or download the repository in a seperate folder, and copy the **tinyxml2.cpp** and **tinyxml2.h** files into the **~/command_bot_ws/src/command_bot/src/** directory.

After we've ensured we have all the proper files, we'll run a make on the repository:
```
catkin_make 
```

The next step involves sourcing the workspace and installing dependancies:
```
source ~/command_bot/devel/setup.bash
cd ~/command_bot_ws 
sudo apt-get update && rosdep update
rosdep install --from-paths src --ignore-src -r -y

```

## Setting up Amazon Lex
Take a moment to look into the **src/command_bot/examples/** directory at a file titled **configuration.yaml**. This file contains the settings that the lex_node will use to find the Chatbot running on the Amazon Lex cloud service. 
In order to utilize the AWS Lex node, the user must have an Amazon Web Service account. Under that account, two things will need to be created:
1. IAM Account access keys
2. Amazon Lex chatbot

To create an access key, search for IAM under services on the AWS developer portal. use an IAM access keys, the user must have the AWS cli installed on their Ubuntu kernel. The AWS cli will be used to locally register Access and Secret Keys, allowing the AWS cloud software to interface with the lex_node. More information on account setup can be found here:
https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html

To create the Amazon Lex chatbot, search for Lex under services on the AWS developer portal. The system will default to Lex v2, but the lex-ros1 node can only interact with Lex v1. The last menu option on the Amazon Lex sidebar is titled "Return to the V1 Console". Clicking this will take you back to Lex v1 and allow us to create a valid chatbot.
Under the Bots page, select *Actions* and then *Import*, and import the command_bot_Export.json file from the **/examples** folder. This will allow the generation of a chatbot that will function correctly with the example Events.xml file included with the Command_bot package.

## Events XML
The command bot determines it's actions based on an Event file that is designed by the user. The Events within the file are customizable, and rely on a trigger system that is actived by **intents** reported by Amazon Lex. In this way, the user can design unqiue relationships between the voice/text commands registered on AWS Lex, and the actions undetaken by the ROS system that command bot is deployed on.
The XML Structure is also follows:
```
<eventList>
    <event name="Name" intent="Lex Intent">
        <requirement type="Unique_Rquirement" value="0-9"></requirement>
        <action type="Unique_Action" value="0-9"></action>
    </event>
<eventList>
```
Utilizing the requirement and action structure, the user can design events that are interdependant and have flexible behavior.

## Running Command Bot
Command bot can be executed by running the included launcher file, the following command will allow this:
```
source ~/command_bot/devel/setup.bash
roslaunch command_bot command_bot_application.launch
```

The **lex_interface_node** that is was designed for command_bot will take user inputs and send them to the **lex_node** and await the response from AWS.
The reponse is then in turn used to trigger a reaction on the **event_runner** node, which will be executing the Events.xml file.
The lex_interface_node listens for information on the following topics:
**/input** - Takes a direct string input for user statements to the robot.
**/voice** - Takes audio sample file path and passes them to AWS lex for interpretation.

Below are some examples of commands that can be sent in the terminal to command_bot:

```
rostopic pub /input std_msgs/String "Go forward"
```

```
rostopic pub /input std_msgs/String "Goodbye"

```
Note: The Goodbye intent here  will **fail**, because the Goodbye intent in the Events.xml has requirements the user must meet first.


```
rostopic pub /input std_msgs/String "Hello"
```
Note: The Hello intent will execute an **action**, because the Hello intent in the Events.xml has action statements attached.

```
rostopic pub /input std_msgs/String "Goodbye"

```
Note: This time, the Goodbye intent succeeded, because the Hello intent fullfilled Goodbye's prerequisits. 
